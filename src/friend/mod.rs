pub mod editor;
pub use editor::EditorView;
mod menubar;
pub use menubar::add_menubar;
mod treeview;
pub use treeview::new_filetree_view;