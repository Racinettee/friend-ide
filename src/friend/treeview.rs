use cursive::{
    Cursive,
    event::{Event, EventResult},
    Printer
};

use cursive_tree_view::{
    Placement,
    TreeView
};

use log::info;

use std::{
    cmp::Ordering,
    fmt, fs, io,
    path::PathBuf,
    str::FromStr
};

#[derive(Clone, Debug)]
pub struct TreeEntry {
    pub name: String,
    pub dir: Option<PathBuf>,
    pub file_dir: Option<PathBuf>
}

impl fmt::Display for TreeEntry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

fn collect_entries(dir: &PathBuf, entries: &mut Vec<TreeEntry>) -> io::Result<()> {
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();

            if path.is_dir() {
                entries.push(TreeEntry {
                    name: entry
                        .file_name()
                        .into_string()
                        .unwrap_or(String::new()),
                    dir: Some(path),
                    file_dir: None
                });
            } else if path.is_file() {
                entries.push(TreeEntry {
                    name: entry
                        .file_name()
                        .into_string()
                        .unwrap_or(String::new()),
                    dir: None,
                    file_dir: Some(dir.join(entry.file_name()))
                });
            }
        }
    }
    Ok(())
}

fn expand_tree(tree: &mut TreeView<TreeEntry>, parent_row: usize, dir: &PathBuf) {
    let mut entries = Vec::new();
    collect_entries(dir, &mut entries).ok();

    entries.sort_by(|a, b| match (a.dir.is_some(), b.dir.is_some()) {
        (true, true) | (false, false) => a.name.cmp(&b.name),
        (true, false) => Ordering::Less,
        (false, true) => Ordering::Greater
    });

    for i in entries {
        if i.dir.is_some() {
            tree.insert_container_item(i, Placement::LastChild, parent_row);
        } else {
            tree.insert_item(i, Placement::LastChild, parent_row);
        }
    }
}

/// Creates a treeview with the purpose of displaying a director that the user can interact with
pub fn new_filetree_view(path: PathBuf, on_item_click: fn(siv: &mut Cursive, item: TreeEntry)) -> TreeView<TreeEntry> {
    let mut tree = TreeView::<TreeEntry>::new()
        .on_submit(move |siv: &mut Cursive, row_id| {
            let entry_data = siv.call_on_name("tree", move |tree: &mut TreeView<TreeEntry>|
                tree.borrow_item(row_id).unwrap().clone()
            ).unwrap();
            on_item_click(siv, entry_data);
        });


    tree.insert_item(
        TreeEntry {
            name: path.file_name().unwrap().to_str().unwrap().to_string(),
            dir: Some(path.clone()),
            file_dir: Some(path.clone())
        }, Placement::After, 0);

    expand_tree(&mut tree, 0, &path);

    tree.set_on_collapse(|siv: &mut Cursive, row, is_collapsed, children| {
        if !is_collapsed && children == 0 {
            siv.call_on_name("tree", move |tree: &mut TreeView<TreeEntry>| {
                if let Some(dir) = tree.borrow_item(row).unwrap().dir.clone() {
                    expand_tree(tree, row, &dir);
                }
            });
        }
    });

    tree
}
