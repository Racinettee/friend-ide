use cursive::{
    Cursive,
    theme::*,
    traits::*,
    direction::{Direction, Orientation},
    event::{Event, EventResult, Key, MouseButton, MouseEvent},
    Printer, Vec2, View,
    views::{Dialog, LinearLayout, ScrollView}
};

use log::{info, error};

use ropey::{
    iter::Lines, Rope
};

use std::{
    fs::File,
    iter::Enumerate,
    io::{BufReader, BufWriter},
    path::PathBuf
};

use super::treeview::TreeEntry;

pub const primary_layout: &str = "primary_layout";
pub const editor_name: &str = "editor";

pub struct EditorView {
    buff: Rope,
    cursor_pos: Vec2,
    path: Option<String>
}

impl EditorView {
    pub fn new() -> EditorView {
        EditorView {
            buff: Rope::from_str(""),
            cursor_pos: Vec2::from((0,1)),
            path: None
        }
    }
    pub fn new_with_str(buff: &str) -> EditorView {
        EditorView {
            buff: Rope::from(buff),
            cursor_pos: Vec2::from((0,1)),
            path: None
        }
    }
    pub fn new_with_file(buff: &str) -> Result<EditorView, std::io::Error> {
        let file = File::open(buff)?;
        let rope = Rope::from_reader(file)?;
        info!("{}", buff);
        Ok(EditorView {
            buff: rope,
            cursor_pos: Vec2::from((0,1)),
            path: Some(String::from(buff))
        })
    }
    /// Callback for the treeview when an item is clicked
    pub fn open_from_item_click(siv: &mut Cursive, entry: TreeEntry) {
        let new_editor = EditorView::new_with_file(entry.file_dir.unwrap().to_str().unwrap());//.expect("Couldn't create new editor");
        if new_editor.is_err() {
            error!("{:?}", new_editor.err());
        } else {
            let new_editor = new_editor.unwrap();
            siv.call_on_name(primary_layout, move |layout: &mut LinearLayout| {
                if layout.len() > 1 {
                    layout.remove_child(1);
                }
                let display_name = new_editor.display_name();
                layout.add_child(Dialog::around(ScrollView::new(new_editor.with_name("editor"))).title(display_name));
            });
        }
    }
    /// Saves the buffer out to file
    pub fn save_buffer(&self) -> Result<(), std::io::Error> {
        if self.path.is_none() {
            // We have to do a save as type routine
        }
        let file = File::create(self.path.as_ref().unwrap())?;
        self.buff.write_to(file)?;
        Ok(())
    }
    /// The name the dialog showing the editor will use
    pub fn display_name(&self) -> String {
        if self.path.is_none() {
            return String::new()
        }
        let path = self.path.clone().unwrap();
        let parts = path.split('/');
        parts.last().unwrap().to_owned()
    }
}

impl View for EditorView {
    fn draw(&self, printer: &Printer) {
        let mut line_print_spot = Vec2::from((0, 0));
        let mut print_cursor = true;
        let len_lines = self.buff.len_lines();
        let mut utf8_encode_buffer = [0; 2]; 
        // Print the lines of text
        for line_idx in 0 .. len_lines {
            let line = self.buff.line(line_idx);
            for (x, ch) in line.chars().enumerate() {
                // If the cursor is atop a character, print the character
                // using the reverse style
                if x == self.cursor_pos.x && line_print_spot.y == self.cursor_pos.y {
                    printer.with_color(ColorStyle::secondary(),
                        |printer| printer.with_effect(Effect::Reverse,
                            |printer| printer.print(line_print_spot, ch.encode_utf8(&mut utf8_encode_buffer))));
                    print_cursor = false
                } else {
                    printer.print(line_print_spot, ch.encode_utf8(&mut utf8_encode_buffer))
                }
                line_print_spot.x += 1
            }
            line_print_spot.y += 1;
            line_print_spot.x = 0
        }
        
        // Print the cursor:
        if print_cursor {
            printer.with_color(ColorStyle::secondary(), |printer| printer.print(self.cursor_pos, "█"));
        }
    }

    // Without this function defined it seems things won't draw
    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        use std::cmp::max;
        //constraint //now we just take whole available space
        let mut constraint = constraint;
        // If we return .y as only self.buff.len we can get an effect where the view only takes up the required space in the terminal
        constraint.y = max(self.buff.len_lines(), constraint.y);
        constraint
    }

    // accept focus if it is passed on by returning true
    fn take_focus(&mut self, _direction: Direction) -> bool {
        true
    }

    fn on_event(&mut self, event: Event) -> EventResult {
        match event {
            Event::Char(ch) => {
                let line_offset = self.buff.line_to_char(self.cursor_pos.y);
                self.buff.insert_char(line_offset + self.cursor_pos.x, ch);
                self.cursor_pos.x += 1;
                return EventResult::Consumed(None)
            },
            Event::Key(Key::Enter) => {
                let line_offset = self.buff.line_to_char(self.cursor_pos.y);
                self.buff.insert_char(line_offset + self.cursor_pos.x, '\n');
                self.cursor_pos.y += 1;
                self.cursor_pos.x = 0;
                return EventResult::Consumed(None)
            }
            Event::Key(Key::Backspace) => {
                // Needs more work to be able to delete 0th character
                let line_offset = self.buff.line_to_char(self.cursor_pos.y);
                let char_offset = line_offset + self.cursor_pos.x;
                if self.cursor_pos.x == 0 {
                    if self.cursor_pos.y != 0 {
                        self.cursor_pos.y -= 1;
                        self.cursor_pos.x = self.buff.line(self.cursor_pos.y).len_chars() - 1
                    }
                } else {
                    self.cursor_pos.x -= 1;
                }
                if self.cursor_pos != Vec2::from((0, 0)) {
                    self.buff.remove(char_offset-1..char_offset);
                }
                return EventResult::Consumed(None)
            },
            Event::Key(Key::Left) => if self.cursor_pos.x > 0 {
                self.cursor_pos.x -= 1;
                return EventResult::Consumed(None)
            },
            Event::Key(Key::Right) => {
                self.cursor_pos.x += 1;
                return EventResult::Consumed(None)
            },
            Event::Key(Key::Up) => if self.cursor_pos.y > 0 {
                self.cursor_pos.y -= 1;
                return EventResult::Consumed(None)
            },
            Event::Key(Key::Down) => {
                self.cursor_pos.y += 1;
                return EventResult::Consumed(None)
            },
            Event::Mouse{offset, position, event} => {
                // If the offset x or y is greater than the position reported
                // ignore the event, otherwise the subtraction to calculate the click
                // position will cause an overflow
                if offset.x > position.x || offset.y > position.y {
                    return EventResult::Ignored
                }
                match event {
                    MouseEvent::Release(btn) => if btn == MouseButton::Left {
                        self.cursor_pos = position - offset;
                        let len_lines = self.buff.len_lines();
                        if self.cursor_pos.y > len_lines {
                            self.cursor_pos.y = len_lines - 1
                        }
                        let len_chars = self.buff.line(self.cursor_pos.y).len_chars();
                        if self.cursor_pos.x > len_chars {
                            self.cursor_pos.x = len_chars
                        }
                        return EventResult::Consumed(None)
                    },
                    _ => { }
                }
            },
            _ => { }
        }
        EventResult::Ignored
    }
}
