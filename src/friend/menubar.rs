use cursive::{
    Cursive,
    event::Key,
    menu::MenuTree,
    views::{Dialog, TextView}
};

use log::info;

use super::{
    editor,
    editor::*
};

fn menu_save(siv: &mut Cursive) {
    siv.call_on_name(editor_name, |editor_view: &mut EditorView| {
        let result = editor_view.save_buffer();
        if result.is_err() {
            info!("Error: {}", result.unwrap_err())
        }
    });
}

/// Asks the user if they want to quit, and proceeds if yes
fn menu_quit(siv: &mut Cursive) {
    siv.add_layer(Dialog::around(TextView::new("Are you sure you want to quit?"))
        .button("Yes", |siv| siv.quit())
        .button("No", |siv| { siv.pop_layer(); }));
}

/// Adds the menu bar items to the menu bar, attached call backs
pub fn add_menubar(siv: &mut Cursive) {
    siv.menubar().add_subtree("File",
        MenuTree::new()
            .leaf("Open", |siv| { siv.add_layer(Dialog::info("Open!")) })
            .leaf("Open Dir", |siv| { siv.add_layer(Dialog::info("Open Dir!")) })
            .leaf("Save", menu_save)
            .leaf("Quit", menu_quit));
    siv.set_autohide_menu(false);
    siv.add_global_callback(Key::Esc, |siv| siv.select_menubar());
}