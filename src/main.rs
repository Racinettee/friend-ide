use backtrace::Backtrace;

use cursive::{
    traits::*,
    views::{
        Dialog, LinearLayout, ScrollView
    }
};

//use rustpython_compiler as compiler;
//use rustpython_vm as vm;
//use rustpython_vm::readline::ReadlineResult::Line;

use std::{env, panic};

mod friend;
use friend::{
    add_menubar,
    editor,
    EditorView,
    new_filetree_view
};

fn main() {
    panic::set_hook(Box::new(|panic_info| {
        let backtrace = Backtrace::new();
        //  Do something with backtrace and panic_info.
        log::error!("{:?}", backtrace);
    }));

    simple_logging::log_to_file("log.txt", log::LevelFilter::Info);

    let mut siv = cursive::default();

    add_menubar(&mut siv);

    let frienditor = EditorView::new_with_str("Hello my baby hello my\ndarling hello my ragtime gal");
    let treeview = new_filetree_view(env::current_dir().expect("Working directory missing."), EditorView::open_from_item_click);

    let file_tree_and_codeview = LinearLayout::horizontal()
        .child(Dialog::around(treeview.with_name("tree")).title("Files"))
        .child(Dialog::around(frienditor.with_name("editor")));

    siv.add_layer(file_tree_and_codeview.with_name(editor::primary_layout));

    siv.run();
}
